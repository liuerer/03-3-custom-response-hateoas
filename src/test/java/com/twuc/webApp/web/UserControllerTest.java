package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_get_user() throws Exception {
        mockMvc
            .perform(get("/users/2"))
            .andExpect(status().is(200))
            .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
            .andExpect(jsonPath("$.id").value("2"))
            .andExpect(jsonPath("$.firstName").value("O_+"))
            .andExpect(jsonPath("$.lastName").value("^_^"));
    }

    @Test
    void should_get_user_links() throws Exception {
        mockMvc
            .perform(get("/users/2"))
            .andExpect(status().is(200))
            .andExpect(jsonPath("$._links.self.href").value("http://localhost/users/2"))
            .andExpect(jsonPath("$._links.edit.href").value("http://localhost/users/2"))
            .andExpect(jsonPath("$._links.getProperty.href").value("http://localhost/users/2/property?name={name}"));
    }

    @Test
    void should_get_user_with_specified_header() throws Exception {
        mockMvc
            .perform(get("/users/2"))
            .andExpect(status().is(200))
            .andExpect(header().string("X-Watermark", "User-2"));
    }
}
